/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

#pragma once
#include "CounterExampleAlgo_IReqAcpt.h"
namespace DESpot
{
	
	CounterExampleAlgo_IReqAcpt::CounterExampleAlgo_IReqAcpt(CntrExmpTupleMap in_tuple,const DesSubsystem& Subsys,DesProject::EventIteratorPtr eventIt)
	{
	m_eventIt=eventIt;
	tranMap=NULL;
	FoundList=NULL;
	main_pendinglist=NULL;
	Level=0;
	m_isFinalState=false;
	runonce=false;
	
		
			int i=0;
			this->m_FinalState.resize(in_tuple.size());

			// Add interface des as plants and  Encoding the error tuple
			DesProject::DesIteratorPtr desIterator=Subsys.getInterface().createDesIterator();
			for(desIterator->first(); desIterator->notDone(); desIterator->next(),i++)
			{				
				m_FinalState[i]=in_tuple[desIterator->currentItem().getName()];
				addInputDes(&desIterator->currentItem());
			}	

			// Add low level des as supervisors and  Encoding the error tuple
			DesProject::DesIteratorPtr SSdesIterator=Subsys.createDesIterator();
			for(SSdesIterator->first(); SSdesIterator->notDone(); SSdesIterator->next(),i++)
			{				
				m_FinalState[i]=in_tuple[SSdesIterator->currentItem().getName()];
				addInputDes(&SSdesIterator->currentItem());
			}
	}


	CounterExampleAlgo_IReqAcpt::~CounterExampleAlgo_IReqAcpt()
	{//base distructor will be invoked automatically and will free the resources
	}
}