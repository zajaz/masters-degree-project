/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

#pragma once
#include "CounterExampleAlgo.h"
namespace DESpot
{
	class CounterExampleAlgo_IntfImpAlgo:public CounterExampleAlgo
	{
	private:
		CounterExampleAlgo_IntfImpAlgo(void);
	public:
		typedef map<std::wstring,short> TupleMap;
		CounterExampleAlgo_IntfImpAlgo(const DesHierProject* in_project,
									   const DesSubsystem* subsys,
									   DesProject::EventIteratorPtr eventIt,
									   TupleMap& in_tuplemap);
		~CounterExampleAlgo_IntfImpAlgo();
	};
}