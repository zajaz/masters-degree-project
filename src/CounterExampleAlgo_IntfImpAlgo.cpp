/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

#pragma once
#include "CounterExampleAlgo_IntfImpAlgo.h"
namespace DESpot
{
	CounterExampleAlgo_IntfImpAlgo::CounterExampleAlgo_IntfImpAlgo(const DesHierProject* in_project,																  
																   const DesSubsystem* subsys,
																    DesProject::EventIteratorPtr eventIt,
																   TupleMap& in_tuplemap)
	{
	m_eventIt=eventIt;		
	tranMap=NULL;
	FoundList=NULL;
	main_pendinglist=NULL;
	Level=0;
	m_isFinalState=false;
	runonce=false;			
		
		int i=0;
		this->m_FinalState.resize(in_tuplemap.size());
		DesSubsystem::DesIteratorPtr desIT=subsys->createDesIterator();
			for(desIT->first(); desIT->notDone(); desIT->next(),i++)
			{				
				m_FinalState[i]=in_tuplemap[desIT->currentItem().getName()];
				addInputDes(&desIT->currentItem());
			}			
		DesSubsystem::DesIteratorPtr IntrfDesIT = subsys->getInterface().createDesIterator();
			for(IntrfDesIT->first(); IntrfDesIT->notDone(); IntrfDesIT->next(),i++)
			{
				m_FinalState[i]=in_tuplemap[IntrfDesIT->currentItem().getName()];
				addInputDes(&IntrfDesIT->currentItem());
			}
	}
		
	CounterExampleAlgo_IntfImpAlgo::~CounterExampleAlgo_IntfImpAlgo()
	{//base distructor will be invoked automatically and will free the resources
	}
				
}