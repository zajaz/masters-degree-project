/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements 
	for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/
#pragma once
#include <deque>
#include <vector>
#include <iostream>
#include "CEStateInfoStructure.h"
using namespace std;

namespace DESpot
{

static short ce_desnum;

typedef std::vector<short> SrcTuple;

struct PendingQueueNode
{
	SrcTuple StateTuple;
	int Level;
	StateInfoNode* StateReference;
	PendingQueueNode():StateTuple(ce_desnum){}
	~PendingQueueNode(){}
};

class PendingQueue
{
public:
	PendingQueue(short DesNum);
	PendingQueue(short DesNum,int Size);
	~PendingQueue();
	void Enque(SrcTuple &tuple, unsigned long level,StateInfoNode* ref);
	StateInfoNode * Deque(SrcTuple &tuple, unsigned long& level);
	inline bool isEmpty()  { return listcounter == 0; }
	inline int itemCount() { return listcounter; }
	inline int blockSize() { return list.size(); }

private:	
	PendingQueue(){}
	void add_new_block();
	void move_to_next_block();
	std::deque<PendingQueueNode*> list;
	long long listcounter;
	int frontIndex;
	int backIndex;
	PendingQueueNode* tail;
	PendingQueueNode* head;
	int block_size;
};

}

