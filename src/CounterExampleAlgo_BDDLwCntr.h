/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

#pragma once
#include "CounterExampleAlgo.h"

namespace DESpot
{
	class CounterExampleAlgo_BDDLwCntr:public CounterExampleAlgo
	{
	private:
		 CounterExampleAlgo_BDDLwCntr(void);
	public:
		 CounterExampleAlgo_BDDLwCntr(const DesHierProject* Project,DesProject::EventIteratorPtr eventIt,BDDHISC::Hisc_ChkInfo& checkInfo);
		 ~CounterExampleAlgo_BDDLwCntr();
	};
}