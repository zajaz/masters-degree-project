/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

#pragma once
#include "CounterExampleAlgo.h"
namespace DESpot
{
	class CounterExampleAlgo_BDDFNB:public CounterExampleAlgo
	{
	private:
		CounterExampleAlgo_BDDFNB(void);
	public:
		CounterExampleAlgo_BDDFNB(DesProject* Project,DesProject::EventIteratorPtr eventIt,BDDSD::SD_NBChkInfo& checkInfo);
		~CounterExampleAlgo_BDDFNB();
	};
}