/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

#pragma once
#include "CounterExampleAlgo.h"
namespace DESpot
{
	class CounterExampleAlgo_FlatCntr:public CounterExampleAlgo
	{
	private:
		CounterExampleAlgo_FlatCntr(void);
	public:
		CounterExampleAlgo_FlatCntr(CntrExmpTupleMap in_tuple,DesProject* Project,DesProject::EventIteratorPtr eventIt);
		~CounterExampleAlgo_FlatCntr();
	};
}