/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

#pragma once
#include "CounterExampleAlgo.h"
namespace DESpot
{
	class CounterExampleAlgo_IReqAcpt:public CounterExampleAlgo
	{
	private:
		CounterExampleAlgo_IReqAcpt(void);
	public:
		CounterExampleAlgo_IReqAcpt(CntrExmpTupleMap in_tuple,const DesSubsystem& SubSys,DesProject::EventIteratorPtr eventIt);
		~CounterExampleAlgo_IReqAcpt();
	};
}