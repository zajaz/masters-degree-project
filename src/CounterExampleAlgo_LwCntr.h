/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

#pragma once
#include "CounterExampleAlgo.h"

namespace DESpot
{
	class CounterExampleAlgo_LwCntr:public CounterExampleAlgo
	{
	private:
		 CounterExampleAlgo_LwCntr(void);
	public:
		 // For high level
		 CounterExampleAlgo_LwCntr(const DesHierProject* Project,const DesSubsystem* Subsys,CntrExmpTupleMap in_tuple,DesProject::EventIteratorPtr eventIt);
		 // For low level
		 CounterExampleAlgo_LwCntr(const DesSubsystem* Subsys,CntrExmpTupleMap in_tuple,DesProject::EventIteratorPtr eventIt);
		 ~CounterExampleAlgo_LwCntr();
	};
}