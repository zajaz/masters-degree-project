/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

#pragma once
#include "CounterExampleAlgo.h"
namespace DESpot
{
	class CounterExampleAlgo_BDDFCntr:public CounterExampleAlgo
	{
	private:
		CounterExampleAlgo_BDDFCntr(void);
	public:
		CounterExampleAlgo_BDDFCntr(DesProject* Project,DesProject::EventIteratorPtr eventIt,BDDSD::SD_CtrlChkInfo& checkInfo);
		~CounterExampleAlgo_BDDFCntr();
	};
}