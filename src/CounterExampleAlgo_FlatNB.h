/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

#pragma once
#include "CounterExampleAlgo.h"
namespace DESpot
{
	class CounterExampleAlgo_FlatNB:public CounterExampleAlgo
	{
	private:
		CounterExampleAlgo_FlatNB(void);
	public:
		CounterExampleAlgo_FlatNB(CntrExmpTupleMap in_tuple,DesProject::DesIteratorPtr desIterator,DesProject::EventIteratorPtr eventIt);
		~CounterExampleAlgo_FlatNB();
	};
}