/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

#pragma once
#include "CounterExampleAlgo_FlatNB.h"
namespace DESpot
{
	
	CounterExampleAlgo_FlatNB::CounterExampleAlgo_FlatNB( CntrExmpTupleMap in_tuple,
														  DesProject::DesIteratorPtr desIterator,
														  DesProject::EventIteratorPtr eventIt)
		
	{
	m_eventIt=eventIt;			
	tranMap=NULL;
	FoundList=NULL;
	main_pendinglist=NULL;
	Level=0;
	m_isFinalState=false;
	runonce=false;
	
			// Encoding the error tuple and adding des in parallel
			int i=0;
			this->m_FinalState.resize(in_tuple.size());
			for(desIterator->first(); desIterator->notDone(); desIterator->next(),i++)
			{				
				m_FinalState[i]=in_tuple[desIterator->currentItem().getName()];	
				addInputDes(&desIterator->currentItem());
			}	
	}
			
	CounterExampleAlgo_FlatNB::~CounterExampleAlgo_FlatNB()
	{//base distructor will be invoked automatically and will free the resources
	}
}