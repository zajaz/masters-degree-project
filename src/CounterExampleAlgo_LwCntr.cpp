/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

#pragma once
#include "CounterExampleAlgo_LwCntr.h"
namespace DESpot
{	
	 CounterExampleAlgo_LwCntr::CounterExampleAlgo_LwCntr(const DesHierProject* Project,
														  const DesSubsystem* Subsys,
														  CntrExmpTupleMap in_tuple,
														  DesProject::EventIteratorPtr eventIt)
	 {
	m_eventIt=eventIt;
	tranMap=NULL;
	FoundList=NULL;
	main_pendinglist=NULL;
	Level=0;
	m_isFinalState=false;
	runonce=false;
		
			int i=0;
			this->m_FinalState.resize(in_tuple.size());

			//Added des from subsystem and encoding the error tuple in parallel
			DesProject::DesIteratorPtr desIterator=Subsys->createDesIterator();
			for(desIterator->first(); desIterator->notDone(); desIterator->next(),i++)
			{				
				m_FinalState[i]=in_tuple[desIterator->currentItem().getName()];
				addInputDes(&desIterator->currentItem());
			}		

		// Add des from all interfaces in the project and encoding the error tuple in parallel
		DesHierProject::InterfIteratorPtr interfIt = Project->createInterfIterator();
		for(interfIt->first(); interfIt->notDone(); interfIt->next())
		{
			const DesInterface& interf = interfIt->currentItem();
			DesProject::DesIteratorPtr IntrfdesIterator=interf.createDesIterator();	
			for(IntrfdesIterator->first(); IntrfdesIterator->notDone();IntrfdesIterator->next(),i++)
			{				
				m_FinalState[i]=in_tuple[IntrfdesIterator->currentItem().getName()];
				addInputDes(&IntrfdesIterator->currentItem());
			}
		}

	
	}
	//=====================================================================================================================		
						  
	 CounterExampleAlgo_LwCntr::CounterExampleAlgo_LwCntr(const DesSubsystem* Subsys,
														  CntrExmpTupleMap in_tuple,
														  DesProject::EventIteratorPtr eventIt)
	
	 {
	m_eventIt=eventIt;
	tranMap=NULL;
	FoundList=NULL;
	main_pendinglist=NULL;
	Level=0;
	m_isFinalState=false;
	runonce=false;
			
			int i=0;
			this->m_FinalState.resize(in_tuple.size());

			// Encoding the error tuple and encoding the error tuple in parallel
			DesProject::DesIteratorPtr desIterator=Subsys->createDesIterator();
			for(desIterator->first(); desIterator->notDone(); desIterator->next(),i++)
			{				
				m_FinalState[i]=in_tuple[desIterator->currentItem().getName()];
				addInputDes(&desIterator->currentItem());
			}		

			// Add des from all interfaces in the project and encoding the error tuple in parallel		
			DesProject::DesIteratorPtr IntrfdesIterator=Subsys->getInterface().createDesIterator();
			for(IntrfdesIterator->first(); IntrfdesIterator->notDone();IntrfdesIterator->next(),i++)
			{				
				m_FinalState[i]=in_tuple[IntrfdesIterator->currentItem().getName()];
				addInputDes(&IntrfdesIterator->currentItem());
			}		
	}
	 //==============================================================================================================//

	 CounterExampleAlgo_LwCntr::~CounterExampleAlgo_LwCntr()
	{//base distructor will be invoked automatically and will free the resources
	}
}