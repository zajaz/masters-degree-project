/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

#pragma once
#include "CounterExampleAlgo_FlatCntr.h"
namespace DESpot
{
	
	CounterExampleAlgo_FlatCntr::CounterExampleAlgo_FlatCntr(CntrExmpTupleMap in_tuple,
															 DesProject* Project,
															 DesProject::EventIteratorPtr eventIt)
	{
	m_eventIt=eventIt;		
	tranMap=NULL;
	FoundList=NULL;
	main_pendinglist=NULL;
	Level=0;
	m_isFinalState=false;
	runonce=false;
		

			// adding des and Encoding the error tuple in parallel
			int i=0;
			DesProject::DesIteratorPtr desIterator=Project->createDesIterator();
			this->m_FinalState.resize(in_tuple.size());	
			for(desIterator->first(); desIterator->notDone(); desIterator->next(),i++)
			{
				addInputDes(&desIterator->currentItem());				
				m_FinalState[i]=in_tuple[desIterator->currentItem().getName()];				
			}		
	}
	
	CounterExampleAlgo_FlatCntr::~CounterExampleAlgo_FlatCntr()
	{			
	}
}