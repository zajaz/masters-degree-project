/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

#pragma once
#include "CounterExampleAlgo.h"
namespace DESpot
{
	class CounterExampleAlgo_LwNB:public CounterExampleAlgo
	{
	private:
		CounterExampleAlgo_LwNB(void);
	public:
		// for high level non blocking
		CounterExampleAlgo_LwNB(const DesHierProject* Project,const DesSubsystem* Subsys,CntrExmpTupleMap in_tuple,DesProject::EventIteratorPtr eventIt);
		// for low level non blocking
		CounterExampleAlgo_LwNB(const DesSubsystem* Subsys,CntrExmpTupleMap in_tuple,DesProject::EventIteratorPtr eventIt);		
		~CounterExampleAlgo_LwNB();
	};
}