/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

#include "CntrExmpPendingQueue.h"

namespace DESpot
{

PendingQueue::PendingQueue(short DesNum):listcounter(0),frontIndex(0),backIndex(0)
{	
	ce_desnum = DesNum;
	block_size = 100000;
	list.clear();
	head = tail = new PendingQueueNode[block_size];
	list.push_back(head);
}

PendingQueue::PendingQueue(short DesNum, int Size):listcounter(0),frontIndex(0),backIndex(0),block_size(Size)
{	
	ce_desnum = DesNum;
	list.clear();
	head = tail = new PendingQueueNode[block_size];
	list.push_back(head);
}

PendingQueue::~PendingQueue()
{
	if(list.empty()) return;
	PendingQueueNode *temp = list.front();
	delete [] temp;
	list.pop_front();	
}

void PendingQueue::Enque(SrcTuple &tuple,unsigned long level,StateInfoNode* state_reference)
{
	for(short i=0;i<ce_desnum;i++) tail->StateTuple[i] = tuple[i];
	tail->Level = level;
	tail->StateReference = state_reference;
	backIndex++;
	listcounter++;
	if(backIndex == block_size)		add_new_block();
	else							tail++;		
}

void PendingQueue::add_new_block()
{
	backIndex = 0;
	tail = new PendingQueueNode[block_size];
	list.push_back(tail);
}

StateInfoNode * PendingQueue::Deque(SrcTuple &tuple,unsigned long& level)
{  
	listcounter--;
	if(listcounter < 0)	std::cout<<"Runtime error in counter example pending queue. Dequeing more elements than enqueued."<<std::endl;
	
	for(short i=0;i<ce_desnum;i++)	tuple[i] = head->StateTuple[i];
	level = head->Level;
	StateInfoNode* Ref = head->StateReference;
	frontIndex++;
	if(frontIndex == block_size)  move_to_next_block();
	else						  head++;

	return Ref;
}

void PendingQueue::move_to_next_block()
{
	frontIndex=0;
	PendingQueueNode* temp = list.front();
	list.pop_front();
	delete [] temp;
	if(!list.empty()) head = list.front();
}

}