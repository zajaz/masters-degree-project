/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

#pragma once
#include "CounterExampleAlgo.h"

namespace DESpot
{
	class CounterExampleAlgo_BDDAnsAcpt:public CounterExampleAlgo
	{
	private:
		 CounterExampleAlgo_BDDAnsAcpt(void);
	public:
		 CounterExampleAlgo_BDDAnsAcpt(const DesHierProject* Project,DesProject::EventIteratorPtr eventIt,BDDHISC::Hisc_ChkInfo& checkInfo);
		 ~CounterExampleAlgo_BDDAnsAcpt();
	};
}