/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

#pragma once
#include "CounterExampleAlgo_IAnsAccept.h"
namespace DESpot
{
	
	CounterExampleAlgo_IAnsAcpt::CounterExampleAlgo_IAnsAcpt(CntrExmpTupleMap in_tuple,DesHierProject* Project,DesProject::EventIteratorPtr eventIt)
	{
	m_eventIt=eventIt;
	tranMap=NULL;
	FoundList=NULL;
	main_pendinglist=NULL;
	Level=0;
	m_isFinalState=false;
	runonce=false;

			int i=0;
			this->m_FinalState.resize(in_tuple.size());
			// Add interface des as plants and  Encode the error state tuple	
			DesHierProject::InterfIteratorPtr interfIt = Project->createInterfIterator();
			for(interfIt->first(); interfIt->notDone(); interfIt->next())
			{
				DesProject::DesIteratorPtr InterfdesIterator = interfIt->currentItem().createDesIterator();							
						for(InterfdesIterator->first(); InterfdesIterator->notDone(); InterfdesIterator->next(),i++)
						{				
							m_FinalState[i]=in_tuple[InterfdesIterator->currentItem().getName()];
							addInputDes(&InterfdesIterator->currentItem());
						}
			}

			// Add high level subsystem des as supervisors and encoding the error state tuple
			DesProject::DesIteratorPtr desIterator=Project->getRootSubsys().createDesIterator();							
					for(desIterator->first(); desIterator->notDone(); desIterator->next(),i++)
					{				
						m_FinalState[i]=in_tuple[desIterator->currentItem().getName()];
						addInputDes(&desIterator->currentItem());
					}		
	}


	CounterExampleAlgo_IAnsAcpt::~CounterExampleAlgo_IAnsAcpt()
	{//base distructor will be invoked automatically and will free the resources
	}
}