/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

#include "CEStateInfoStructure.h"
namespace DESpot
{
	StateInfoTree::StateInfoTree()
	{	
		block_size=100000;
		list.clear();
		next_available_node = new StateInfoNode[block_size];
		list.push_back(next_available_node);
	}

	StateInfoTree::StateInfoTree(int size)
	{   
		block_size = size;
		list.clear();
		next_available_node=new StateInfoNode[block_size];
		list.push_back(next_available_node);
	}

	StateInfoTree::~StateInfoTree()
	{
		while(!list.empty())
		{
			StateInfoNode *temp=list.front();
			delete [] temp;
			list.pop_front();
		}
	}

	StateInfoNode* StateInfoTree::AllocateNode(void)
	{	
		StateInfoNode* temp = next_available_node;	
		Index++;
		if(Index == block_size)		add_block();
		else						next_available_node++;
		return temp;
	}
	
	void StateInfoTree::add_block()
	{
		Index=0;
		next_available_node = new StateInfoNode[block_size];
		list.push_back(next_available_node);
	}
}