/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

/**DESC:
This module creates a dummy project for a subsystem of a hierarchical project and
in parallel decodes a state tuple to be loaded into the simulator.

Input:
(1) Subsytem to be converted.
(2) A map of Des names and the ID of the state each des is in.

Output:
(1) Dummy project
(2) Decoded state tuple. (A vector containing refrences to the states)

Advanced usage:
If only dummy project is need, pass in an empty tuple map.
**/

#pragma once
#include "DesFlatProject.h"
#include "DesHierProject.h"
#include "DesSubsystem.h"
#include "CounterExampleAlgo.h"
#include "DesProject.h"
#include "DesSerializer.h"
namespace DESpot
{	
	class DummyProject
	{
	private:
		const DesSubsystem* m_ErrorSubSystem;
		const DesHierProject* m_ErrorProject;
		DesFlatProject* DummyProjPtr;
		SimCompState::StateTuple SimulationErrorTuple;
		typedef map<std::wstring,short> TupleMap;
	public:
		inline SimCompState::StateTuple& getSimulationErrorTuple(){return SimulationErrorTuple;} 
	private:
		DummyProject();
	public:
		
		//its for simulation error tuple generation of a complete project, and does not create a dummyproject
		DummyProject(const DesProject* in_project,TupleMap in_tuple);
		// for low level subsystem
		DummyProject(const DesSubsystem* in_subsystem,TupleMap in_tuple,const DesHierProject* in_project);

		//for high level subsystem
		DummyProject(const DesSubsystem* in_subsystem,const DesHierProject* in_project,TupleMap in_tuple);
		~DummyProject();		
		DesProject* GetDummyProject();
		Des& clone(Des& existing_des);
	};
	
	//DesProject* HiscSubSystemToFlat(DesHierProject& HierProject);
}