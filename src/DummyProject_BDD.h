/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/
/**

DESC:
This module creates a dummy project and a simulation error state tuple in parallel.

**/

#pragma once
#include "DesFlatProject.h"
#include "DesHierProject.h"
#include "DesSubsystem.h"
#include "CounterExampleAlgo.h"
#include "DesProject.h"
#include "DesSerializer.h"
namespace DESpot
{	
	class DummyProject_BDD
	{
	private:
		const DesSubsystem* m_ErrorSubSystem;
		const DesHierProject* m_ErrorProject;
		DesProject* DummyProjPtr;
		SimCompState::StateTuple SimulationErrorTuple;
		
	public:
		DummyProject_BDD();
		// for flat projects
		DummyProject_BDD(const DesProject* in_project,BDDSD::SD_ChkInfo& checkInfo);
		// for Hierarchical projects
		DummyProject_BDD(const DesProject* in_project,BDDHISC::Hisc_ChkInfo& checkInfo);
		// for low level subsystem
		DummyProject_BDD(const DesSubsystem* in_subsystem,BDDHISC::Hisc_ChkInfo& checkInfo,DesProject* in_project);
		//for high level subsystem
		DummyProject_BDD(const DesSubsystem* in_subsystem,const DesHierProject* in_project,BDDHISC::Hisc_ChkInfo& checkInfo);
		~DummyProject_BDD();		
		DesProject* GetDummyProject();
		inline SimCompState::StateTuple& getSimulationErrorTuple(){return SimulationErrorTuple;} 
		Des& clone(Des& existing_des);
	};
	
	//DesProject* HiscSubSystemToFlat(DesHierProject& HierProject);
}