/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

#pragma once
#include "CounterExampleAlgo_BDDFCntr.h"

namespace DESpot
{
	
	CounterExampleAlgo_BDDFCntr::CounterExampleAlgo_BDDFCntr(DesProject* Project,DesProject::EventIteratorPtr eventIt,BDDSD::SD_CtrlChkInfo& checkInfo)
	{
	m_eventIt=eventIt;	
	tranMap=NULL;
	FoundList=NULL;
	main_pendinglist=NULL;
	Level=0;
	m_isFinalState=false;
	runonce=false;
		
		int i=0;	
		this->m_FinalState.resize(checkInfo.CE_tuplemap.size());
		bool StateFound=false;
		DesProject::DesIteratorPtr desIT=Project->createDesIterator();
			for(desIT->first(); desIT->notDone(); desIT->next(),i++)
			{
				StateFound=false;
				QString tempName = QString::fromStdWString(desIT->currentItem().getName());
				std::string DesName=tempName.toStdString();
				addInputDes(&desIT->currentItem());
				if(checkInfo.CE_tuplemap.find(DesName)==checkInfo.CE_tuplemap.end())
				{
					QString Terr ="Fatal Error: The error tuple provided by the algorithm did not have an entry for the DES \"%1\". Please inform DESpot developers.";
					QString DesNameErr=Terr.arg(QString::fromStdString(DesName));
					QMessageBox::critical(NULL, "DESpot",DesNameErr);	
				}

				DESpot::Des::StateIteratorPtr stateitr = desIT->currentItem().createStateIterator();

					for(stateitr->first();stateitr->notDone();stateitr->next())
					{	
						if(stateitr->currentItem().getName() == checkInfo.CE_tuplemap[DesName])
						{
							m_FinalState[i]=stateitr->currentItem().getId();
							StateFound=true;
							break;
						}
						

					}
					if(!StateFound)
						{
							QString tempError = "The current state name \"%1\" was not found in the statepool of \"%2\" DES. Please inform the DESpot developers";
							QString Statename = QString::fromStdWString(checkInfo.CE_tuplemap[DesName]);
							QString DESName	= QString::fromStdString(DesName);
							QString StateLookupError=tempError.arg(Statename,DESName);							
							QMessageBox::critical(NULL, "DESpot",StateLookupError);	
						}

			}		
	}

	
	CounterExampleAlgo_BDDFCntr::~CounterExampleAlgo_BDDFCntr()
	{//base distructor will be invoked automatically and will free the resources
	}
}