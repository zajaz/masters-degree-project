/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/

#pragma once
#include "CounterExampleAlgo.h"
namespace DESpot
{
	class CounterExampleAlgo_IAnsAcpt:public CounterExampleAlgo
	{
	private:
		CounterExampleAlgo_IAnsAcpt(void);
	public:
		CounterExampleAlgo_IAnsAcpt(CntrExmpTupleMap in_tuple,DesHierProject* Project,DesProject::EventIteratorPtr eventIt);
		~CounterExampleAlgo_IAnsAcpt();
	};
}