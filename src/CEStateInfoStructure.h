/*	Author: Zain Ajaz
	Supervisor: Dr. Ryan Leduc
	
	Project created in conformity with the requirements for the Degree of Master of Engineering in Software Engineering, 
	Computing and Software Department, 
	McMaster University
	2012 - 2013

*/
#pragma once
#include <deque>
#include <vector>
#include <iostream>
using namespace std;

namespace DESpot
{

struct StateInfoNode
{
  short Event;
  StateInfoNode * prev;
};

class StateInfoTree
{
public:	
	StateInfoTree();
	StateInfoTree(int size);     
	~StateInfoTree();
	StateInfoNode* AllocateNode(void);

private:
	std::deque<StateInfoNode*> list;
	int Index;
	StateInfoNode* next_available_node;
	void add_block();
	int block_size;
};

}

