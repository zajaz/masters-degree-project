Author: Zain Ajaz
Supervisor: Dr. Ryan Leduc	

Project created in conformity with the requirements 
for the Degree of Master of Engineering in Software Engineering, 
Computing and Software Department, 
McMaster University
2012 - 2013

Title: �Debugging DES in DESpot� 

My contributions:
� Extended DESpot for debugging Discrete Event Systems (modeled as automata) 
� Developed a solution with:
	- Divide and conquer approach: Localized debugging to avert state explosion 
	- Blocked memory allocation: Increased cache hits
�	Designed an algorithm to find the shortest path from start state tuple to 
	error state tuple
�	Programming Language: C++, GUI Framework: Qt 4.5.2
